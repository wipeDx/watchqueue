import { Entry } from './Entry';

export class Queue {
    id: number;
    name: string;
    entries: number[];
}