import { Component, OnInit } from '@angular/core';
import { faArrowUp, faArrowDown, faTrash } from "@fortawesome/free-solid-svg-icons";
import { EntryService } from '../entry.service';
import { Entry, QueueEntry } from '../Entry';
import { Queue } from '../Queue';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.css']
})
export class QueueComponent implements OnInit {

  faArrowUp = faArrowUp;
  faArrowDown = faArrowDown;
  faTrash = faTrash;
  queueID: number;
  queue: Entry[];
  queues: Queue[];


  constructor(
    private entryService: EntryService
  ) { }

  ngOnInit() {
    this.getQueues();
    this.getQueue(1);
  }

  getQueue(queueID: number): void {
    this.entryService.getEntries(queueID)
        .subscribe(entries => {this.queue = entries; this.queueID = queueID;});
  }

  getQueues(): void {
    this.entryService.getQueues()
        .subscribe(queues => this.queues = queues);
  }

  moveEntryUp(queueID: number, entryPosition: number): void {
    this.entryService.getSwappableQueueEntries(queueID, entryPosition, entryPosition-1, false)
      .subscribe(entries => this.swapPosition(entries));
  }

  moveEntryDown(queueID: number, entryPosition: number): void {
    this.entryService.getSwappableQueueEntries(queueID, entryPosition, entryPosition+1, true)
      .subscribe(entries => this.swapPosition(entries));
  }

  swapPosition(entries: QueueEntry[]) {
    if (entries.length != 2) {
      console.log("TOO MANY / NOT ENOUGH ENTRIES");
      return;
    }
    var tempPos;
    tempPos = entries[0].position;
    entries[0].position = entries[1].position;
    entries[1].position = tempPos;
    entries.forEach(e => {
      this.entryService.updateQueuePosition(e).subscribe(() => this.getQueue(this.queueID));
    });
  }

  removeOneFromPosition(entries: QueueEntry[]) {
    for (var i = 0; i < entries.length; i++) {
      entries[i].position = entries[i].position - 1;
      this.entryService.updateQueuePosition(entries[i]).subscribe(() => {
        this.getQueue(this.queueID);
        console.log("Got the queue");
      })
    }
  }

  deleteEntry(queueID: number, entryToDelete: Entry): void {
    // Get entries  with position > entryPosition
    this.entryService.getEntriesAfterPosition(queueID, entryToDelete.position)
      .subscribe(entries => {
        // delete entry with entryPosition
        this.entryService.deleteEntryFromQueue(queueID, entryToDelete)
          // update following entries with removeOneFromPosition
          .subscribe(() => this.removeOneFromPosition(entries));
      });
  }

}
