export class Entry {
    id: number;
    name: string;
    IMDB: string;
    length: string;
    position: number; // Queue Position
}

export class QueueEntry {
    id: number; // QueueID
    entryID: number; // EntryID
    position: number; // Queue Position
}