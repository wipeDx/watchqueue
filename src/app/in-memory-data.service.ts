import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Entry } from './Entry';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const entries = [
      { id: 0, name: "The Godfather", IMDB: "tt0068646" },
      { id: 1, name: "Pulp Fiction", IMDB: "tt0110912" }
    ];
    const queue = [
      { id: 0, entries: [1, 0]}
      ];
    return {entries, queue};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(entries: Entry[]): number {
    return entries.length > 0 ? Math.max(...entries.map(entry => entry.id)) + 1 : 0;
  }
}