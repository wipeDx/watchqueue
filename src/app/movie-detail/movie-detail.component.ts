import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { EntryService } from '../entry.service';
import { Entry } from '../Entry';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  public movie: Entry;

  constructor(
    private route: ActivatedRoute,
    private entryService: EntryService,
    private location: Location
    ) { }

  ngOnInit(): void {
    this.getMovie();
  }

  getMovie(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.entryService.getEntry(id)
        .subscribe(entry => this.movie = entry[0]);
  }

}
