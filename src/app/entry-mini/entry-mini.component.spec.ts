import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryMiniComponent } from './entry-mini.component';

describe('EntryMiniComponent', () => {
  let component: EntryMiniComponent;
  let fixture: ComponentFixture<EntryMiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryMiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
