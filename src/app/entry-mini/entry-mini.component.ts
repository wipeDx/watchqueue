import { Component, OnInit } from '@angular/core';
import { EntryService } from '../entry.service';
import { Entry } from '../Entry';
import { Input } from "@angular/core";

@Component({
  selector: 'app-entry-mini',
  templateUrl: './entry-mini.component.html',
  styleUrls: ['./entry-mini.component.css']
})

export class EntryMiniComponent implements OnInit {
  @Input() offset: number = 0;
  entry: Entry;

  constructor(private entryService: EntryService) {  }

  ngOnInit() {
    this.getNextUpEntry();
  }

  getNextUpEntry(): void {
    this.entryService.getNextUpEntry(1, this.offset)
        .subscribe(entry => this.entry = entry[0]);
    
  }

}
