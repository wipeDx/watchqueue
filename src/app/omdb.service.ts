import { Injectable } from '@angular/core';
import { API_KEY } from './api-key';
import { Entry } from "./Entry";
import { HttpClient, HttpHeaders } from '@angular/common/http';

const apiURL: string = "https://www.omdbapi.com/?apikey=" + API_KEY;

@Injectable({
  providedIn: 'root'
})

export class OmdbService {

  

  constructor(private http: HttpClient) {  }

  public get_entry(imdb_id: string): Entry {
    var fullURL = apiURL + "&i=" + imdb_id;
    // var entry: Entry;
    var result = this.http.get(fullURL);
    console.log(result);
    return {id: 0, IMDB: imdb_id, name: "Blah ", length: "0:00", position: 1};
  }
}
