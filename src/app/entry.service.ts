import { Injectable } from '@angular/core';
import { Entry, QueueEntry } from './Entry';
import { Observable, of } from 'rxjs';
import { flatMap, catchError, map, tap } from "rxjs/operators";
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Queue } from './Queue';

@Injectable({
  providedIn: 'root'
})
export class EntryService {

  private queueEntriesUrl = 'http://192.168.42.6:3000/watchqueue/public/queueEntries';
  private entriesUrl = 'http://192.168.42.6:3000/watchqueue/public/entries';
  private queuesURL = 'http://192.168.42.6:3000/watchqueue/public/queues';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private messageService: MessageService,
    private http: HttpClient
    ) { }

  ///////////////////
  // GET FUNCTIONS //
  ///////////////////

  /**
   * Simply fetches all entries from the API
   */
  getEntries(id: number): Observable<Entry[]> {
    return this.http.get<Entry[]>(this.queueEntriesUrl + `?queueEntries.id=${id}&_join=inner:entries:queueEntries.entryID:$eq:entries.id&_order=position`)
            .pipe(
              tap(_ => this.log('Fetched all entries')),
              catchError(this.handleError<Entry[]>('getEntries', []))
            );
  }

  /**
   * Simply fetches the queue from the API
   */
  getQueue(id: number): Observable<Queue> {
    
    return this.http.get<Queue>(this.queueEntriesUrl + `?id=${id}`)
            .pipe(
              tap(_ => this.log('Fetched queue')),
              catchError(this.handleError<Queue>('getEntries', {id: 0, name: "Empty Queue", entries:[]}))
            );
  }

  /**
   * Gets the specified element from the API.
   * If offset is greater than the queue, return nothing.
   * Sadly gives an entry into the message system
   * @param offset - offset from the first element
   */
  getNextUpEntry(queueid: number = 0, offset: number = 0): Observable<Entry> {
    // get the queue and pipe the first result of the queue back into the actual request
    return this.getQueue(queueid)
            .pipe(
              flatMap(queue => {
                return this.http.get<Entry>(this.queueEntriesUrl + `?queueEntries.id=${queueid}&position=${1+offset}&_join=inner:entries:queueEntries.entryID:$eq:entries.id`)
                  .pipe(
                    tap(_ => this.log(`Fetched next entry (offset ${offset})`)),
                    catchError(this.handleError<Entry>(`getNextUpEntry (offset ${offset})`, {} as Entry))
                  )
                }
              )         
            );
  }

    getSwappableQueueEntries(queueID: number = 0, pos1: number, pos2: number, moveDown: boolean): Observable<QueueEntry[]> {
        var order = moveDown ? "" : "-";
        // return this.getQueue(queueID)
        //     .pipe (
        //         flatMap(queue => {
                    
        //         })
        //     );
        return this.http.get<QueueEntry[]>(this.queueEntriesUrl + `?id=${queueID}&position=$gte.${moveDown? pos1 : pos2}&position=$lte.${moveDown? pos2 : pos1}&_order=${order}position`)
            .pipe(
                tap(_ => this.log(`Fetched 2 entry to be able to swap positions`)),
                catchError(this.handleError<QueueEntry[]>(`getSwappableQueueEntries`, {} as QueueEntry[]))
            );
    }

  /**
   * UNTESTED!
   * Grabs an entry of a specific queue at a specific position
   * @param queueid id of queue to query
   * @param position position of entry to query
   */
  getQueueEntryByPos(queueid: number = 0, position: number = 0): Observable<QueueEntry> {
    // get the queue and pipe the first result of the queue back into the actual request
    return this.getQueue(queueid)
            .pipe(
              flatMap(queue => {
                return this.http.get<QueueEntry>(this.queueEntriesUrl + `?queueEntries.id=${queueid}&position=${position}&_join=inner:entries:queueEntries.entryID:$eq:entries.id`)
                  .pipe(
                    tap(_ => this.log(`Fetched entry of queue (queueid: ${queueid}, position ${position})`)),
                    catchError(this.handleError<QueueEntry>(`getQueueEntryByPos (queueid: ${queueid}, position ${position})`, {} as QueueEntry))
                  )
                }
              )         
            );
  }

  /**
   * Fetches entry by id.
   * 404's when not found.
   * @param id id of entry to get
   */
  getEntry(id: number): Observable<Entry> {
    const url = `${this.entriesUrl}?id=${id}`;
    return this.http.get<Entry>(url)
            .pipe(
              tap(_ => this.log(`Fetched entry id ${id}`)),
              catchError(this.handleError<Entry>(`getEntry id ${id}`))
            )
  }

  /**
   * 
   * @param entry 
   */
  getQueues(): Observable<Queue[]> {
      return this.http.get<Queue[]>(this.queuesURL)
                .pipe(
                    tap(_ => this.log("Fetched all queues")),
                    catchError(this.handleError<Queue[]>(`getQueues`, [{id: 1, name: "Cool Queue", entries: [1,2]}]))
                );
  }

  getEntriesAfterPosition(queueID: number, position: number): Observable<QueueEntry[]> {
      return this.http.get<QueueEntry[]>(this.queueEntriesUrl + `?id=${queueID}&position=$gt.${position}&_order=position`)
        .pipe(
            tap(_ => this.log(`Fetched all entries of queue ${queueID} after position ${position}`)),
            catchError(this.handleError<QueueEntry[]>(`getQueues`, []))
        );
  }

  ///////////////////
  // PUT FUNCTIONS //
  ///////////////////

  updateQueuePosition(entry: QueueEntry): Observable<any> {
    return this.http.put(this.queueEntriesUrl + `?id=${entry.id}&entryID=${entry.entryID}`, entry, this.httpOptions)
        .pipe(
            tap(_ => this.log(`updated queue entry position`)),
            catchError(this.handleError<any>('updateQueuePositions'))
        );
  }

  ////////////////////
  // POST FUNCTIONS //
  ////////////////////

  addEntryToQueue() {
  }

  addEntryToSystem() {
    /* TODO: 
     * Check if it's in the system
     * If not: Search from OMDB Service
     * Add it into system
     */ 
  }

  //////////////////////
  // DELETE FUNCTIONS //
  //////////////////////

  deleteEntryFromQueue(queueID: number, entry: Entry): Observable<QueueEntry>  {
    return this.http.delete<QueueEntry>(this.queueEntriesUrl + `?id=${queueID}&entryID=${entry.id}`, this.httpOptions)
        .pipe(
            tap(_ => this.log(`deleted queueEntry (id ${entry.id}) from queue ${queueID}`)),
            catchError(this.handleError<QueueEntry>('deleteEntryFromQueue'))
        );
  }



  /**
   * Handle HTTP Operation that failed
   * Lets the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send error to remote logging infrastructure
      console.error(error); // log to console for now

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning empty result.
      return of(result as T);
    }
  }

  private log(message: string) {
    this.messageService.add(`EntryService: ${message}`);
  }
}
